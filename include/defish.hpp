/*
 * =====================================================================================
 *
 *       Filename:  defish-paranomic.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2018년 08월 21일 11시 58분 01초
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef DEFISH_H
#define DEFISH_H

#include <opencv2/core.hpp>

class Remapper
{
	private:
		cv::Mat mapX;
		cv::Mat mapY;

	public:
		explicit Remapper(cv::Mat mX, cv::Mat mY);

		void buildImage(cv::Mat &src, cv::Mat &dest);
		void operator()(const cv::Mat &src, cv::Mat &dst);
};

class Defish
{
	private:
		const int height;
		const int width;

	public:
		explicit Defish(const cv::Mat &mat);
		explicit Defish(const cv::Size2i srcSize);

		Remapper paranomic();
		Remapper pinhole(float strength=1.f, const float zoom=1.f);
		Remapper window(const float alpha, const float beta, const float theta, const float zoom,
						const cv::Size2i winSize=cv::Size2i(500,500));
};


//static void errorExit(const char *format, const char *arg)
//{
//    fprintf(stderr, "Error: ");
//    fprintf(stderr, format, arg);
//    fprintf(stderr, "\nexit.\n");
//    exit(-1);
//}

#endif // DEFISH_H
