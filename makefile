CXX  = /bin/g++
#CXX  = g++-7
CXXFLAGS = -std=c++17
OBJS = main.o defish.o
SRCS = $(OBJ:.o=.cpp)
TARGET = defish

LIBS = -L/usr/local/lib64
FLAGS = -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_imgproc
INC = -I$(PWD)/include

.SUFFIXES : .o .cpp

$(TARGET): $(OBJS)
	$(CXX) -o $@ $^ $(LIBS) $(FLAGS)
	rm -f $(OBJS)

.o .cpp :
	$(CXX) -c $(INC) $<

run: $(TARGET)
	./$(TARGET)

clean:
	rm -f $(TARGET) $(OBJS)

# DO NOT DELETE

main.o: ./include/defish.hpp 
defish.o: ./include/defish.hpp 
	cd src; $(CXX) -c defish.cpp; mv defish.o ..
