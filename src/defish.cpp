/*
 * =====================================================================================
 *
 *       Filename:  defish.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  2018년 09월 05일 00시 01분 13초
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  YOUR NAME (), 
 *   Organization:  
 *
 * =====================================================================================
 */

#include "../include/defish.hpp"

#include <opencv2/imgproc.hpp>

#include <cmath>

Remapper::Remapper(cv::Mat mX, cv::Mat mY)
	: mapX(std::move(mX)), mapY(std::move(mY))
{

}
void
Remapper::operator()(const cv::Mat &src, cv::Mat &dst)
{
	cv::remap(src, dst, mapX, mapY,
			cv::INTER_LINEAR, cv::BORDER_CONSTANT, cv::Scalar(0, 0, 0));
}

Defish::Defish(const cv::Mat &mat)
	: height(mat.rows), width(mat.cols)
{

}
Defish::Defish(const cv::Size2i srcSize)
	: height(srcSize.height), width(srcSize.width)
{

}

Remapper 
Defish::paranomic()
{
	int midy = height / 2;
	int midx = width  / 2;
	int maxmag = (midy > midx ? midy : midx);
    auto circum = static_cast<int>(2 * M_PI * maxmag);     // c = 2*pi*r

	cv::Mat mapX(maxmag, circum, CV_32FC1);
	cv::Mat mapY(maxmag, circum, CV_32FC1);

	for (int y = 0; y < maxmag; y++) {
		for (int x = 0; x < circum; x++) {
			double theta = -1.0 * x / maxmag;       // -(x * 2.0 * M_PI / width);
			double mag = maxmag - y;                // y * 1.0 * maxmag / height;
			auto targety = static_cast<double>(midy + mag * cos(theta));
			auto targetx = static_cast<double>(midx + mag * sin(theta));

			mapX.at<float>(y, x) = targetx;
			mapY.at<float>(y, x) = targety;
		}
	}

	return Remapper(mapX, mapY);
}

Remapper
Defish::pinhole(float strength, const float zoom)
{
	const int halfHeight = height/2;
	const int halfWidth  = width/2;
	
	if(0.0f == strength) strength = 0.0001f;

	cv::Mat mapX(height, width, CV_32FC1);
	cv::Mat mapY(height, width, CV_32FC1);

	auto corrRadius = sqrt(height*height + width*width)/static_cast<double>(strength);

	for(int y=0;y<height;++y){
		for(int x=0;x<width;++x){
			auto yd = static_cast<double>(zoom*(y - height/2));
			auto xd = static_cast<double>(zoom*(x - width/2));
			auto rd = sqrt(yd*yd + xd*xd)/ corrRadius;
			double theta;

			if(0==rd)
				theta = 1.0;
			else
				theta = atan(rd)/rd;

			auto targety = static_cast<double>(halfHeight + theta*yd);
			auto targetx = static_cast<double>(halfWidth  + theta*xd);

			mapX.at<float>(y, x) = targetx;
			mapY.at<float>(y, x) = targety;
		}
	}

	return Remapper(mapX, mapY);
}
Remapper
Defish::window(const float alpha, const float beta, const float theta, const float zoom,
			   const cv::Size2i winSize)
{
	const auto & dWidth  = winSize.width;
	const auto & dHeight = winSize.height;
	cv::Mat mapX(dHeight, dWidth, CV_32FC1);
	cv::Mat mapY(dHeight, dWidth, CV_32FC1);

	// # Set the angle parameters
	// # Build the fisheye mapping
	const float al = alpha / 180.0;
	const float be = beta  / 180.0;
	const float th = theta / 180.0;
	const float R  = width / 2.0f;

	const float A = cosf(th) * cosf(al) - sinf(th) * sinf(al) * cosf(be);
	const float B = sinf(th) * cosf(al) + cosf(th) * sinf(al) * cosf(be);
	const float C = cosf(th) * sinf(al) + sinf(th) * cosf(al) * cosf(be);
	const float D = sinf(th) * sinf(al) - cosf(th) * cosf(al) * cosf(be);
	const float mR = zoom * R;
	const float mR2 = mR * mR;
	const float mRsinBesinAl = mR * sinf(be) * sinf(al);
	const float mRsinBecosAl = mR * sinf(be) * cosf(al);
	const auto centerV = static_cast<int>(dHeight / 2.0);
	const auto centerU = static_cast<int>(dWidth  / 2.0);
	const float centerY = height / 2.0;
	const float centerX = width / 2.0;

	for (int absV = 0; absV < dHeight; absV++) {
		float v = absV - centerV;
		float vv = v * v;
		for (int absU = 0; absU < dWidth; absU++) {
			float u = absU - centerU;
			float uu = u * u;
			float upperX = R * (u * A - v * B + mRsinBesinAl);
			float lowerX = sqrtf(uu + vv + mR2);
			float upperY = R * (u * C - v * D - mRsinBecosAl);
			float lowerY = lowerX;
			float x = upperX / lowerX + centerX;
			float y = upperY / lowerY + centerY;
			int _v = centerV <= v ? v : v + centerV;
			int _u = centerU <= u ? u : u + centerU;
			mapX.at<float>(_v, _u) = x;
			mapY.at<float>(_v, _u) = y;
		}
	}

	return Remapper(mapX, mapY);
}
