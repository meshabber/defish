#include <opencv2/opencv.hpp>
#include "./include/defish.hpp"

const std::string path {"./Images/"};
const std::string file {"camera2.jpg"};

int main()
{
	cv::Mat img = cv::imread(path+file);

	cv::Mat img2;
	auto M = cv::getRotationMatrix2D(
			cv::Point2f(img.cols/2, img.rows/2),
			45, 1);
	cv::warpAffine(img, img2, M, img.size());
	//cv::imwrite(path+"ww.jpg", img2);

	cv::Mat out;

	struct windowParameter{
		float alpha;
		float beta;
		float theta;
		float zoom;
	};

	std::vector<windowParameter> params {
		{-270.f,  135.f, 270.f, 0.3f}, // left
		{-270.f, -135.f, 270.f, 0.3f}, // right
		{	0.f,   90.f,   0.f, 0.3f}, // up
		{	0.f,  -90.f,   0.f, 0.3f}, // down
		{-270.f, 	0.f, 270.f, 0.3f}, // mid
	};
	
	Defish defish(img);

	auto paranomic = defish.paranomic();
	paranomic(img, out);
	cv::imwrite(path+"defish-para.jpg", out);

	auto pinhole = defish.pinhole(4.3);
	pinhole(img, out);
	cv::imwrite(path+"defish-pin.jpg", out);

	std::vector<Remapper> winmaps1;
	std::vector<Remapper> winmaps2;
	int cnt = 1;
	for(auto &&param : params){
		auto &&[x, y, z, zoom] = param;

		winmaps1.push_back(defish.window(x,y,z,zoom, {700, 700}));
	}
	for(auto &&param : params){
		auto &&[x, y, z, zoom] = param;

		winmaps2.push_back(defish.window(x,y,z,zoom));
	}	winmaps2.pop_back();
	for(auto &&map : winmaps1){
		map(img, out);
		cv::imwrite(path+"window"+std::to_string(++cnt)+".jpg", out);
	}
	for(auto &&map : winmaps2){
		map(img2, out);
		cv::imwrite(path+"window"+std::to_string(++cnt)+".jpg", out);
	}
  
	return 0;
}
