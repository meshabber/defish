#-------------------------------------------------
#
# Project created by QtCreator 2018-08-20T07:14:24
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = CVtest
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0
QMAKE_CXX = g++-7
CONFIG += c++1z

QMAKE_CXXFLAGS += -std=c++1z

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    cvlabel.cpp \
    cvimage.cpp \
    ../src/defish.cpp \
    captureqthread.cpp

HEADERS += \
        mainwindow.h \
    cvlabel.h \
    cvimage.h \
    ../include/defish.hpp \
    captureqthread.h

FORMS += \
        mainwindow.ui

#LIBS += -L/home/morning/storage/git/opencv/3.4_build/lib \
#        -lopencv_core -lopencv_imgproc -lopencv_highgui -lopencv_imgcodecs
LIBS += -L/usr/local/lib \
#LIBS += -L/usr/local/lib64 \
        -lopencv_core -lopencv_imgproc -lopencv_imgcodecs \
        -lopencv_videoio
#LIBS += `pkg-config --libs opencv`

INCLUDEPATH += /usr/local/include
INCLUDEPATH += $$PWD/../include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
