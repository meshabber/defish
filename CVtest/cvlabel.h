#ifndef CVLABEL_H
#define CVLABEL_H

#include <QLabel>

class CVImage;

class CVLabel : public QLabel
{
    Q_OBJECT
public:
    explicit CVLabel(QLabel *parent = nullptr);

public slots:
    void setCVImage(const CVImage &);

private:
    QImage buf;
};

#endif // CVLABEL_H
