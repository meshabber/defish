#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "cvlabel.h"

#include "../include/defish.hpp"

#include "captureqthread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

private slots:
    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::MainWindow *ui;

    CVLabel *src;
//    CVLabel *dst;
    std::array<CVLabel*, 11> dsts;

    std::vector<Remapper> maps;

    std::array<cv::Mat, 11> outs;
    cv::Mat buffer;
    cv::Mat img2;
    cv::Mat out;

    CaptureQThread * thd;
};

#endif // MAINWINDOW_H
