#ifndef CAPTUREQTHREAD_H
#define CAPTUREQTHREAD_H

#include <QThread>

namespace cv{
class Mat;
class VideoCapture;
};

class CaptureQThread : public QThread
{
    Q_OBJECT
public:
    CaptureQThread(const std::string & srcPath ,cv::Mat * sharedBuffer);
    ~CaptureQThread() override;

private:
    cv::VideoCapture *cap;
    cv::Mat			 *buffer;

protected:
    void run() override;

signals:
    void updateImage();
};

#endif // CAPTUREQTHREAD_H
