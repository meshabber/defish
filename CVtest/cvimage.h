#ifndef CVIMAGE_H
#define CVIMAGE_H

#include <QImage>

#include <opencv2/core.hpp>

/*
 * WARNING:	[runtime] cv::Mat had no virtual destructor, might have trouble on below code..
 *	cv::Mat* foo = new CustomMat;
 *  delete foo;
 */
class CVImage : public cv::Mat
{
public:
    explicit CVImage(const cv::Mat &);

//    CVImage &operator=(const cv::Mat &);

    ~CVImage();

    static QImage mat2qimg(const cv::Mat &);
    const  QImage toQImage() const;

private:
};

#endif // CVIMAGE_H
