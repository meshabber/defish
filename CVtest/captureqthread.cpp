#include "captureqthread.h"

#include <opencv2/core.hpp>
#include <opencv2/videoio.hpp>

CaptureQThread::CaptureQThread(const std::string & srcPath, cv::Mat * sharedBuffer) :
      buffer(sharedBuffer)
{
    cap = new cv::VideoCapture(srcPath);
    if(!cap->isOpened()) { this->exit(-1); }
}

CaptureQThread::~CaptureQThread()
{
    delete cap;
}

void CaptureQThread::run()
{
    for(;;)
    {
        if(!cap->read(*buffer)) { break; }

        emit updateImage();
    }
}
