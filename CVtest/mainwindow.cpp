#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/opencv.hpp>

#include "cvimage.h"

const std::string path = "../Video/";

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    thd(new CaptureQThread(path+"short.mp4", &buffer))
{
    ui->setupUi(this);
    src = new CVLabel(ui->lbl_src);

    connect(thd, &CaptureQThread::updateImage, [=](){
        src->setCVImage(CVImage(buffer));
    });

    thd->start();

    cv::Mat out;
    Defish fish(cv::Size2i{1440, 1080});

    struct windowParameter{
        float alpha;
        float beta;
        float theta;
        float zoom;
    };

    std::vector<windowParameter> params {
        {-270.f,  135.f, 270.f, 0.3f}, // left
        {-270.f, -135.f, 270.f, 0.3f}, // right
        {	0.f,   90.f,   0.f, 0.3f}, // up
        {	0.f,  -90.f,   0.f, 0.3f}, // down
        {-270.f, 	0.f, 270.f, 0.3f}, // mid
    };

    for(auto && param : params)
    {
        auto && [x, y, z, zoom] = param;

        Remapper map = fish.window(x, y, z, zoom);
        maps.push_back(map);
    }
    for(auto && param : params)
    {
        auto && [x, y, z, zoom] = param;

        Remapper map = fish.window(x, y, z, zoom);
        maps.push_back(map);
    }
        maps.pop_back();

        maps.push_back(fish.paranomic());
        maps.push_back(fish.pinhole(4.3f));

        dsts[0] = new CVLabel(ui->lbl_dst_1);
        dsts[1] = new CVLabel(ui->lbl_dst_2);
        dsts[2] = new CVLabel(ui->lbl_dst_3);
        dsts[3] = new CVLabel(ui->lbl_dst_4);
        dsts[4] = new CVLabel(ui->lbl_dst_5);
        dsts[5] = new CVLabel(ui->lbl_dst_6);
        dsts[6] = new CVLabel(ui->lbl_dst_7);
        dsts[7] = new CVLabel(ui->lbl_dst_8);
        dsts[8] = new CVLabel(ui->lbl_dst_9);

        dsts[9]  = new CVLabel(ui->lbl_paranomic);
        dsts[10] = new CVLabel(ui->lbl_pinhole);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete src;
}

void MainWindow::on_comboBox_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "") {;
    } else if(arg1 == "Paranomic")
    {
        for(auto i=9;i<11;i++){
            maps[i](buffer, out);
            dsts[i]->setCVImage(CVImage(out));
        }
        ui->stackedWidget->setCurrentIndex(0);
    }
    else if(arg1 == "Window")
    {
        for(auto i=0;i<5;i++){
            maps[i](buffer, out);
            dsts[i]->setCVImage(CVImage(out));
        }
        for(auto i=5;i<9;i++){
            maps[i](buffer, out);
            dsts[i]->setCVImage(CVImage(out));
        }
        ui->stackedWidget->setCurrentIndex(1);
    }
}
