# defish

fisheye 렌즈의 왜곡을 보정하는 라이브러리


#### Todo list
- Basic algorithm
  - [x] remapper functor : 성능 향상x20 over
  - [x] Analyze window algorithm 
  - [ ] Combine data structure(remapper & cv::Mat)
  - [ ] Find a suitable windowPrameters..
- GUI
  - [x] Set a default layout
  - [x] Synchronize CVLable and output cv::Mat
  - [ ] Structing CVLable and output cv::Mat
- Threading
  - [x] image output structure
  - [ ] image input(rstp, QThreadPool?)
- Code Analyzing
  - [ ] DarkProgramer's Camera calibration
  - [x] fisheye-window

